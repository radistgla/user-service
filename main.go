package main

import (
	"fmt"
	"log"

	pb "gitlab.com/radistgla/user-service/proto/user"

	"github.com/micro/go-micro"
)

func main() {
	db, err := CreateConnection()
	defer db.Close()

	if err != nil {
		log.Fatalf("Could not connect to database: %v", err)
	}

	db.AutoMigrate(&pb.User{})

	repo := &UserRepository{db}

	srv := micro.NewService(
		micro.Name("shippy.service.user"),
		micro.Version("latest"),
	)

	srv.Init()

	pb.RegisterUserServiceHandler(srv.Server(), &service{repo})

	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}
}
